'use strict';

/* NOTES:

Delete all
    sessionStorage.clear();
*/

// Alias
Storage.prototype.Delete = function(key) {
    this.removeItem(key);
}

Storage.prototype.Save = function(key, data){
    if(data.isArray()){
        this.setArray(key, data);
    }else if(data.isObject()){
        this.object(key, data);
    }else{
        this.setItem(key, data);
    }
};

Storage.prototype.Load = function(key){
    var data = this.getItem(key);
    if(data.isEmpty()) return data;
    if(data[0] == '[' || data[0] == '{') return JSON.parse(data);
    return data;
};

//###########################################################
// Storage Object
//###########################################################

Storage.prototype.object = function(name, object){
    if(object){
        this.setItem(name, JSON.stringify(object));
        if (this === window.localStorage) {
            this.increment('Modified');
        }        
    }else{
        var object = this.getItem(name);
        return (object == undefined) ? {} : JSON.parse(object);        
    }
};

// Object as Hash
Storage.prototype.oKey = function(name, key, value) {
    var object = this.object(name);
    if(value){
        object[key]=value;
        this.object(name, object);
    }else{
        return object[key];        
    }
};
Storage.prototype.setObj = Storage.prototype.object;
Storage.prototype.getObj = Storage.prototype.object;

//###########################################################
// Storage Integrer
//###########################################################
Storage.prototype.getInt = function(key){
    var string = this.getItem(key);
    var i = parseInt(string);
    if(isNaN(i)){
        if(string === undefined || string === null){
            i = 0;
        }
    }
    return i;
};

Storage.prototype.increment = function(key){
    var i = this.getInt(key);
    i++;
    this.setItem(key, i);
};

//###########################################################
// Storage Array
//###########################################################

// Set / Save a complete array
Storage.prototype.setArray = function(key, o) {
//console.log('a: ' + key +' -> '+ o);
    this.setItem(key, JSON.stringify(o));
    if (this === window.localStorage ) {
        this.increment('Modified');
    }
};

Storage.prototype.getArray = function(key) {
    var array = this.getItem(key);
    if(array === undefined || array === null){
        return [];
    }
    return JSON.parse(array);
};

// Add a single key value or array to an array
Storage.prototype.addArray = function(key, value) {
    var array = this.getArray(key);
    array.pushUnique(value);
    this.setArray(key, array);
};

// Delete a single key from an array
Storage.prototype.delArray = function(key, item) {
    var array = this.getArray(key);
    var i = array.indexOf(item);
    if(i != -1){
        array.splice(i, 1);
        this.setArray(key, array);
    }
};

Storage.prototype.inArray = function(key, item) {
    // key is database key
    // item is value in the array
    var array = this.getArray(key);
    if(array.indexOf(item) == -1){
        return false;
    }
    return true;
};

Storage.prototype.countArray = function(key) {
    var array = this.getArray(key);
    return array.length;
};
