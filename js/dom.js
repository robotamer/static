'use strict';

if(typeof $ != 'object') var $ = {};
$.dom = {};

$.dom.e = function (e){
    return (e.isString()) ? document.getElementById(e) : e;
};
$.dom.txt = function (s){
    return document.createTextNode(s);
};

$.dom.AppendSpace = function (to, count){
    to = $.dom.e(to);
    if(count){
        for (var i = count; i >= 0; i--) {
            var s = $.dom.txt('\u00a0');
            to.appendChild(s);
        };
    }else{
        var s = $.dom.txt(' ');
        to.appendChild(s);            
    }
};

$.dom.Append = function (to, type, count){
    to = $.dom.e(to);
    if(count){
        for (var i = count; i >= 0; i--) {
            var s = document.createElement(type);
            to.appendChild(s);
        };
    }else{
        var s = document.createElement(type);
        to.appendChild(s);            
    }
};

$.dom.Link = function(title, url, target) {
    var a = document.createElement('a');
    var t = $.dom.txt(title);
    a.setAttribute('href', url);
    if (target) a.setAttribute("target", "_blank");
    a.appendChild(t);
    return a;
};

// $.dom.AppenedText(document.body, 'hello world', 'h3');
// $.dom.AppenedText('content', 'hello you', 'h3');
$.dom.AppenedText = function(to, txt, type) {
    to = $.dom.e(to);
    txt = $.dom.txt(txt);
    if(type){
        var e = document.createElement(type);
        e.appendChild(txt);
        txt = e;
    }
    to.appendChild(txt);
};

$.dom.AppenedChild = function(to, e) {
    to = $.dom.e(to);
    to.appendChild(e); 
};

$.dom.PrependChild = function(to, e) { 
    to = $.dom.e(to);
    to.insertBefore(e, to.firstChild); 
};

$.dom.RemoveChildren = function(e) {
    e = $.dom.e(e);
    while (e.firstChild) {
        e.removeChild(e.firstChild);
    }
};

$.dom.Remove = function (e){
    e = $.dom.e(e);
    if(e != document.body){
        e.parentNode.removeChild(e);
    }else{
        $.dom.RemoveChildren(document.body);
    }
};

$.dom.RemoveParent =function (e){
    $.dom.Remove(e.parentNode);
};

$.dom.Replace = function (eOld, eNew){
    eOld = $.dom.e(eOld);
    var p = eOld.parentNode;
    p.removeChild(eOld); 
    p.appendChild(eNew);
};

$.dom.ClearText = function(thefield) {
    if (thefield.defaultValue == thefield.value) thefield.value = '';
};

$.dom.Date = function(json) {
    var d = new Date(json);
    var t = document.createElement('time');
    t.setAttribute("datetime", d.toISOString());
    t.appendChild($.dom.txt(d.toLocaleString()));
    return t;
};
$.dom.DateSince = function(json) {
    var d = new Date(json);
    var t = document.createElement('time');
    t.setAttribute("datetime", d.toISOString());
    t.appendChild($.dom.txt($.date.since(d)));
    return t;
};

$.dom.Show = function(id) {
    if (id.isString()) {
        document.getElementById(id).style.display = 'block';
    } else {
        id.style.display = 'block';
    }
};

$.dom.Hide = function(id) {
    var e;
    if (id.isString()) {
        e = document.getElementById(id);
        $.dom.Hide(e);
    } else if(id.isArray()){
        for (var i = 0, l = id.length; i < l; i++) {
            e = document.getElementById(id[i]);
            $.dom.Hide(e);
        }
    } else {
        if(id.style.display != 'none'){
            id.style.display = 'none';
        }
    }
};

$.dom.ShowHide = function(e) {
    e = $.dom.e(e);
    if (e.style.display == 'block') {
        e.style.display = 'none';
    } else {
        e.style.display = 'block';
    }
};
