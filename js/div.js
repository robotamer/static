

function Div(w, h, t, l, p, m, bg) {
    var body = document.getElementsByTagName('body')[0];
    this.div = document.createElement('div');
    this.text = 'No Text';
    this.w = w + 'px';
    this.h = h + 'px';
    this.top = t + 'px';
    this.left = l + 'px';
    this.pad = p + 'px';
    this.margin = m;
    this.bg = '#' + bg;
    this.hide = function() {
        var e = document.getElementById('alert');
        e.style.display = 'none'
    };
    this.show = function() {
        var e = document.getElementById('alert');
        e.style.display = 'block'
    };
    this.set = function() {
        body.appendChild(this.div)
    };
    this.del = function() {
        var e = document.getElementById('alert');
        body.removeChild(e)
    };
    this.define = function() {
        this.div.innerText = this.div.textContent = this.text;
        this.div.id = 'alert';
        this.div.style.position = 'absolute';
        this.div.style.textAlign = 'center';
        this.div.style.border = '1px solid #000';
        this.div.style.width = this.w;
        this.div.style.height = this.h;
        this.div.style.top = this.top;
        this.div.style.left = this.left;
        this.div.style.padding = this.pad;
        this.div.style.margin = this.margin;
        this.div.style.background = this.bg;
        this.div.onclick = this.del();
    };
    this.define()
}