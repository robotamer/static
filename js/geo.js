window.x = 'false';

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
    } else { 
        window.x = "Geolocation is not supported by this browser.";
    }
}

function showPosition(position) {
    window.x = "Latitude: " + position.coords.latitude + " Longitude: " + position.coords.longitude;	
}

function showError(error) {
    switch(error.code) {
        case error.PERMISSION_DENIED:
            window.x = "User denied the request for Geolocation.";
            break;
        case error.POSITION_UNAVAILABLE:
            window.x = "Location information is unavailable.";
            break;
        case error.TIMEOUT:
            window.x = "The request to get user location timed out.";
            break;
        case error.UNKNOWN_ERROR:
            window.x = "An unknown error occurred.";
            break;
    }
}

getLocation();

document.body.appendChild(document.createTextNode(window.x));
