'use strict';

function Cookie(name, value, end) {
    var expires;
    this.Name = function () {
        return name
    };
    this.Value = function (x) {
        if (x) {
            this.value = x;
            this.save()
        }
        return this.value
    };
    this.IsSet = function () {
        return this.value ? true : false
    };
    this.Expires = function (vEnd) {
        if (vEnd) {
            switch (vEnd.constructor) {
            case Number:
                this.expires = vEnd === Infinity ? '; expires=Fri, 31 Dec 9999 23:59:59 GMT' : '; max-age=' + vEnd;
                break;
            case String:
                this.expires = '; expires=' + vEnd;
                break;
            case Date:
                this.expires = '; expires=' + vEnd.toUTCString();
                break
            }
        }
    };
    this.Del = function () {
        this.value = '';
        this.expires = '; expires=Thu, 01 Jan 1971 00:00:00 GMT';
        document.cookie = name + '=' + this.value + this.expires + '; path=/'
    };
    this.save = function () {
        end ? this.Expires(end) : this.expires = '';
        document.cookie = name + '=' + this.value + this.expires + '; path=/'
    };
    if (value) {
        this.Value(value)
    } else {
        var nameEQ = name + '=';
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ')
                c = c.substring(1, c.length);
            if (c.indexOf(nameEQ) == 0) {
                this.value = c.substring(nameEQ.length, c.length)
            }
        }
    }
}
