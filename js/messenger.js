'use strict';
if(typeof $ != 'object') var $ = {};

/************************************
 * Messenger
 * http://jsfiddle.net/q4kcnmoe/

  var log = new Messenger();
  log.Info('my info');
  log.Error('my error');
*************************************/
function Messenger() {
    var self = this;

    this.setup = function() {
        self.e_ul = document.getElementById("messanger");
        if (self.e_ul === null) {
            self.e_ul = document.createElement('ul');
            self.e_ul.setAttribute('id', 'messanger');
            self.e_ul.style.display = 'block';
            self.e_ul.style.listStyleType = 'none';
            self.e_ul.style.position = 'absolute';
            self.e_ul.style.right = '1em';
            self.e_ul.style.top = '1em';
            self.e_ul.style.zIndex = '1000';
            document.body.appendChild(self.e_ul);
        }
    };

    this.makeList = function(text) {
        var e = document.createElement('li');
        e.style.padding = '2px 10px 2px 10px';
        e.appendChild(document.createTextNode(text));
        // e.onmouseover = this.hide(e);
        var delay = text.length * 200 + 2000;
        setTimeout(function() {
            self.e_ul.removeChild(e);
        }, delay);
        return e;
    };

    this.Error = function(text) {
        console.log('[ERROR] ' + text);
        self.setup(text);
        var li = this.makeList(text);
        li.className = "alert";
        li.style.background = '#FF0000';
        li.style.color = '#FFFFFF';
        self.e_ul.appendChild(li);
    };

    this.Info = function(text) {
        console.log('[INFO] ' + text);
        self.setup();
        var li = this.makeList(text);
        li.className = "info";
        li.style.background = '#12B7E4';
        li.style.color = '#000000';
        self.e_ul.appendChild(li);
    };

    this.hide = function (e){
        e.style.display = 'none';
    };
}
$.log = new Messenger();
