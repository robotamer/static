'use strict';

function formData(form){
    var r = {};
    var i = 0;
    for (i = 0; i < form.length; ++i) {
        if (form[i] !== undefined) {
            if (form[i].name) {
                var field_type = form[i].type.toLowerCase();
                switch (field_type) {
                    case "radio":
                    case "checkbox":
                        r[form[i].name] = form[i].checked;
                        break;
                    case "select-multi":
                        r[form[i].name] = form[i].selectedIndex;
                        break;
                    default:
                        r[form[i].name] = form[i].value.trim();
                }
            }
        }
    }
    return r;
}

function formClear(form) {
    for (var i = form.length - 1; i >= 0; i--) {
        if (form[i] !== undefined) {
            if (form[i].name) {
                form[i].value = '';
            }
        }
    }
}

