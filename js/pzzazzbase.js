'use strict'
if(typeof $ != 'object') var $ = {};
$.pzzazz = {};

//$.db = window.sessionStorage;
$.db = window.localStorage;

/***** Bookmarklet *****
javascript:window.location=%22http://localhost:9999/wire.html?u=%22+encodeURIComponent(document.location)+%22&t=%22+encodeURIComponent(document.title)
**************************************************************/
/*
javascript:(function() {
function se(d) {
    return d.selection ? d.selection.createRange().text : d.getSelection()
}
s = se(document);
for (i=0; i<frames.length && !s; i++) s = se(frames[i].document);
if (!s || s=='') s = prompt('Enter%20search%20terms%20for%20Wikipedia','');
open('http://en.wikipedia.org' + (s ? '/w/index.php?title=Special:Search&search=' + encodeURIComponent(s) : '')).focus();
})();
*/

$.src.Pages = [];
$.src.getPages = function (){
	if( ! $.src.Pages.isEmpty() ) return $.src.Pages;
	var links = document.getElementsByClassName('content');
	for (var i = 0, l = links.length; i < l; i++) {
		$.src.Pages.push(links[i].id);
	}
	return $.src.Pages;
};

$.pzzazz.tabCtrl = function(link){
	if(!link) link = $.getUrlHash();
	if(link.isEmpty()) return;
	$.src.getPages();
	if($.src.Pages.has(link) == false) return;
	var link  = document.getElementById(link);
 	var links = document.getElementsByClassName('content');
	for (var i = 0, l = links.length; i < l; i++) {
		if(links[i].id == link.id){
			link.style.display = 'block';
		}else if(links[i].style.display != 'none'){
		    links[i].style.display = 'none';
		}
	}	
};

window.addEventListener('hashchange', function(){
	$.pzzazz.tabCtrl();
});

$.pzzazz.ControlPanal = function(that){
	console.log(that);
    var settings = localStorage.object('settings');
    var field_type = that.type.toLowerCase();
    switch (field_type) {
        case that.name == '':
        	break;
        case 'radio':
        case 'checkbox':
            settings[that.name] = that.checked;
            break;
        case 'select':
            settings[that.name] = that.selectedIndex;
            break;
        default:
            settings[that.name] = that.value;
    }

    if(that.name == 'lang'){
    	$.i18n.Switch(that.value);
    	console.log('lang value: ' + that.value);
    }

    localStorage.object('settings', settings); 
};

$.pzzazz.Delete = function(cat){

	var r = confirm(_('ays'));
	if (r == false) {
	    return;
	} 

	switch(cat) {
		case 'all':
			sessionStorage.clear();
			localStorage.clear();
			$.log.Info('Database deleted');
			break;
		case 'cache':
			sessionStorage.clear();
			$.log.Info('Cache cleared');
			break;
		case 'star': // Keep stared items
			var wires = $.db.getArray('wires');
			for (var i = wires.length - 1; i >= 0; i--) {
			    var wire = $.db.getArray(wires[i]);
			    var vote = $.db.inArray('votes', wire);
			    if(vote != true){
			    	$.db.del(wire);
			    }
			}
			$.log.Info('Everything but stared items deleted');
			break;
	}
};



$.pzzazz.setup = function (){
	var t = $.getUrlVars();
	WireAdd(t['t'], t['u'], t['k'], t['d']);

	var settings = localStorage.object('settings');
	if (settings.isEmpty()){
		var j = '{"google":true,"searx":true,"faroo":true,"bing":true,"first":false,"save":false}';
	    settings = JSON.parse(j);
		settings['version'] = 0.2;
		settings['id'] = $.ruid(8);
		settings['lang'] = 'en';
		$.db.object('settings', settings);
	}
	var keys = Object.keys(settings);
	for (var i = keys.length - 1; i >= 0; i--) {
		var key = keys[i];
		var e  = document.getElementById('settings-'+key);
		if(e){
			if(e.type == 'checkbox' || e.type == 'radio'){
				e.checked = settings[key];				
			}			
		}
	}
	$.i18n.init(settings['lang']);
	$.search.Reload();
	$.pzzazz.tabCtrl();

	var w = new Wire();
	w.Wires();
	w.keywordsReset();
	w.keywordsSort();
	w.filterLoad();

	$.search.SuggestInit();
};

$.pzzazz.plugin = {};

$.pzzazz.plugin.Reset = function (){
	$.pzzazz.plugin.dbKeys = [];
	$.pzzazz.plugin.dbIndex = [];

	$.pzzazz.plugin.AddKey('settings');
	// Belongs to Search
	$.pzzazz.plugin.AddKey('history');
	// Belongs to Wire
	$.pzzazz.plugin.AddKey(['wires', 'tags', 'votes', 'keywords','keywordCount']);
	$.pzzazz.plugin.AddIndex('wires');
};

// add string or array of database keys
$.pzzazz.plugin.AddKey = function (xkey){
	console.log('[PLUGIN] Adding database key: ' + xkey);
	$.pzzazz.plugin.dbKeys.pushUnique(xkey);
};
$.pzzazz.plugin.AddIndex = function (xkey){
	var x = $.db.getArray(xkey);
	if(x.isArray()){
		$.pzzazz.plugin.AddKey(x);
	}else{
		$.log.Error('[PLUGIN] Database Index is not an Array');
	}
};

$.pzzazz.export = function (e){
	var data = {};
	var jsonData = '';
	$.pzzazz.plugin.Reset();

	for (var j = $.pzzazz.plugin.dbKeys.length - 1; j >= 0; j--) {
		var key = $.pzzazz.plugin.dbKeys[j];
		var o = $.db.getItem(key);
		if( o == null) continue;
		console.log('key:' + key + ' o:' + o);
		if( o[0] == '[' || o[0] == '{') { // is Array or Object
			data[key] = JSON.parse(o);
		}else{
			data[key] = o; // is String or int
		}
	}
	jsonData = JSON.stringify(data);
	var blob = new Blob([jsonData], {type: "text/plain;charset=utf-8"});
	jsonData = data = undefined;
	saveAs(blob, "pzzazz.db.json");
	$.pzzazz.plugin.dbKeys = [];
	$.pzzazz.plugin.dbIndex = [];
};

$.pzzazz.import = function (e){
	var e = $.dom.e(e);
	var json = e.value.trim();
	if(json[0] != '{' || json[json.length-1] != '}'){
		$.log.Error('Bad import format');
		return ;
	}
	var data = JSON.parse(json);
	var keys = Object.keys(data);
	for (var i = keys.length - 1; i >= 0; i--) {			
		var key = keys[i];
		console.log("Importing: " + key);
		$.db.Save(key, data[key]);
	}
	$.pzzazz.setup();
	$.log.Info('Data Imported');
};
$.pzzazz.setup();

/*
$.pzzazz.importold = function (e){


	if (!window.File && !window.FileReader && !window.FileList && !window.Blob) {
		console.log('[ERROR] FileReader Not Available');
		$.log.Error('FileReader Not Available');
		return
	}
	if(e == undefined) var e = document.getElementById('dataFile');

	//var file = e.target.files[0];
	var file = e.files[0];
	if (!file) {
		console.log('[ERROR] No file selected');
		$.log.Error('No file selected');
		return;	
	} 

	var reader = new FileReader();
	console.log('[INFO] reader: ' + reader);

	reader.onerror = function(e) {
		console.log('[ERROR][FileReader] ' + e);
	};
	reader.onabort = function(e) {
		console.log('[ABORT][FileReader] ' + e);
	};
	reader.onloadstart = function(e) {
		console.log('[OnStart][FileReader] ' + e);
	};
	reader.onload = function(e) {
		console.log('[INFO][FileReader] Loading file');
		var json = e.result;
		$.db.setItem("json",json);
		var data = JSON.parse(json);
		var keys = Object.keys(data);
		for (var i = keys.length - 1; i >= 0; i--) {			
			var key = keys[0];
			$.db.save(key, data[key]);
		}
		$.log.Info('Data Imported');
	};
};
*/

