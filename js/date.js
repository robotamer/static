'use strict';

if(typeof $ != 'object'){
    var $ = {};
}

function DATE() {

    this.now = function() {
        return new Date();
    };

    this.iso = function (d){
        if(d == undefined) var d = new Date();
        return d.toISOString();
    }

    this.timeUnix = function(timestamp) {
        var r;
        if (timestamp) {
            var x = new Date(timestamp * 1000);
            var yr = x.getFullYear();
            var mo = x.getMonth() + 1;
            var dy = x.getDate();
            var hr = x.getHours();
            var mi = x.getMinutes();
            var sc = x.getSeconds();
            r = yr + '-' + mo + '-' + dy + ' ' + hr + ':' + mi + ':' + sc
        } else {
            r = Math.round(new Date().getTime() / 1000)
        }
        return r
    };

    this.format = function(jsdate) {
        if (jsdate === undefined) jsdate = new Date();
        var days = new Array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
        var months = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
        var date = ((jsdate.getDate() < 10) ? "0" : "") + jsdate.getDate();
        var year = jsdate.getYear();
        year = (year < 1000) ? year + 1900 : year;
        return days[jsdate.getDay()] + ", " + months[jsdate.getMonth()] + " " + date + ", " + year;
    };

    this.fromJson = function(json) {
        return new Date(json);
    };

    this.sinceJson = function(json) {
        var d = new Date(json);
        return this.since(d);
    };

    this.since = function(dateFrom) {

        var interval, seconds;

        seconds = Math.floor((new Date() - dateFrom) / 1000);

        interval = Math.floor(seconds / 31536000);
        if (interval > 1) return interval + " years";

        interval = Math.floor(seconds / 2592000);
        if (interval > 1) return interval + " months";

        interval = Math.floor(seconds / 86400);
        if (interval > 1) return interval + " days";

        interval = Math.floor(seconds / 3600);
        if (interval > 1) return interval + " hours";

        interval = Math.floor(seconds / 60);
        if (interval > 1) return interval + " minutes";

        return Math.floor(seconds) + " seconds";
    };
}
$.date = new DATE();
