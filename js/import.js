'use strict'
if(typeof $ != 'object') var $ = {};

/*

TODO:
    1. Add get Json
    2. Add lezy load (Load only if requested in script) as in src


$.src.domain(["localhost", "pzzazz.com", "www.pzzazz.com"]);
$.src.async();
$.src.js("FileSaver i18n tools messenger form store dom date md5 ajax pzzazzwire pzzazzsearch".split(' '));
$.src.css(["pzzazz", "animation"], "/font/fontello/css/");
$.src.defer();
$.src.js("pzzazzbase");

*/

$.src = {};
$.src.reset = function(){
    $.src._path = undefined;
    $.src._ext = undefined;
    $.src._boolean = undefined;
};
$.src.async = function (){
    $.src._boolean = 'async';
};
$.src.defer = function (){
    $.src._boolean = 'defer';
};
$.src.css = function (name, path){
    $.src._path = (path) ? path : '/css/';
    $.src._ext = 'css';
    $.src.array(name);
};
$.src.js = function (name, path){
    $.src._path = (path) ? path : '/js/';
    $.src._ext = 'js';
    $.src.array(name);
};
$.src.json = function (argument){
    /* body... */
};
$.src.array = function(name){
    if(Object.prototype.toString.call(name) == '[object Array]'){
        for (var i = 0, l = name.length;  i < l; i++) {
            $.src.load(name[i]);
        }
    }else{
        $.src.load(name);
    }
    $.src.reset();
};
$.src.load = function (name){
    if( ! $[name]){
        $.src.file(name);
    }
};
$.src.file = function (name){
    var s;
    var link = 'http://static.' + $.src.domain() + $.src._path + name + '.' + $.src._ext;
    if($.src._ext == 'js'){
        s = document.createElement('script');
        s.setAttribute('src', link);
        s.setAttribute('type', 'text/javascript');
        if($.src._boolean){
            s.setAttribute($.src._boolean, $.src._boolean);
        }
    }else if($.src._ext == 'css'){
        s = document.createElement('link');
        s.setAttribute('href', link);
        s.setAttribute('rel', 'stylesheet');
        s.setAttribute('type', 'text/css');
    }else{
        console.log('import failed');
        return;
    }
    if($.src._boolean == 'defer'){
        document.onreadystatechange = function () {
            if (document.readyState == "complete") {
                document.body.appendChild(s);
            }
        };
    }else{
        if(!document.head){
            document.head = document.getElementsByTagName('head')[0];
        }
        document.head.appendChild(s);
    }
};
$.src.domain = function (list){
    if($.src._d) return $.src._d;
    for (var i = 0, l = list.length;  i < l; i++) {
        if(window.location.hostname.indexOf(list[i]) > -1) {
            $.src._d = list[i];
            return $.src._d;
        }
    }
};
$.src.loaded = function (name){
    if(Object.prototype.toString.call(name) == '[object Array]'){
        for (var i = 0, l = name.length;  i < l; i++) {
            $.src.loadedCheck(name[i]);
        }
    }else{
        $.src.loadedCheck(name);
    }
};

