'use strict'

if(typeof $ != 'object') var $ = {};

// Aliases
$.db = window.localStorage;

$.logger = {};
$.logger.original = console.log;
$.logger.On = function(){
    window.console.log = $.logger.original;
};
$.logger.Off = function(){
    window.console.log = function(){};
};

$.Domain = function () {
    var d = ['localhost', 'pzzazz.com', 'riky.net', 'robotamer.com'];
    var l = d.length;
    for (var i = 0;  i < l; i++) {
        if(window.location.hostname.indexOf(d[i]) > -1) {
            return d[i];
        }
    }
    return null;
};

if($.Domain() != 'localhost'){
    $.logger.Off();
}else{
    console.log('Logger is On');
}

// Takes the Javascript file name and adds it to the document as a script
function include(name, top) {
    var s = document.createElement("script");
    var host;
    if(window.location.hostname.indexOf('localhost') > -1) {
        host = 'http://static.localhost';
    }else{
        host = 'http://static.pzzazz.com';
    }
    
    s.setAttribute("src", host + '/js/' + name + '.js');
    s.setAttribute("type", "text/javascript");
    if(top){
        document.head.appendChild(s);
    }else{
        document.onreadystatechange = function () {
            if (document.readyState == "complete") {
                document.body.appendChild(s);
            }
        };
    }
}


// http://jsfiddle.net/pazd2vab/
// var add = uniqueId();
$.uid = function () {
    var c = 0;
    return function () {
        return c += 1;
    };
};

if (typeof Object.isEmpty === 'undefined') {
    Object.prototype.isEmpty = function () {

        // null and undefined are "empty"
        if (this === null || this === undefined) return true;

        // Assume if it has a length property with a non-zero value
        // that that property is correct.
        if (this.length > 0)    return false;
        if (this.length === 0)  return true;

        // Otherwise, does it have any properties of its own?
        // Note that this doesn't handle
        // toString and valueOf enumeration bugs in IE < 9
        for (var key in this) {
            if (Object.prototype.hasOwnProperty.call(this, key)) return false;
        }
        return true;
    };
}

if (typeof Object.isObject === 'undefined') {
    Object.prototype.isObject = function () {
        return Object.prototype.toString.call(this) == '[object Object]';
    };
}

if (typeof Object.isFunction === 'undefined') {
    Object.prototype.isFunction = function () {
        return Object.prototype.toString.call(this) == '[object Function]';
    };
}

if (typeof Object.isDate === 'undefined') {
    Object.prototype.isDate = function () {
        return Object.prototype.toString.call(this) == '[object Date]';
    };
}

if (typeof Object.isString === 'undefined') {
    Object.prototype.isString = function () {
        return Object.prototype.toString.call(this) == '[object String]';
    };
}

if (typeof Object.isArray === 'undefined') {
    Object.prototype.isArray = function () {
        return Object.prototype.toString.call(this) == '[object Array]';
    };
}

if (typeof Object.isNumber === 'undefined') {
    Object.prototype.isNumber = function () {
        return Object.prototype.toString.call(this) == '[object Number]';
    };
}

if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str){
        return this.slice(0, str.length) == str;
    };
}
if (typeof String.prototype.endsWith != 'function') {
    String.prototype.endsWith = function (str){
        return this.slice(-str.length) == str;
    };
}
// First, checks if it isn't implemented yet.
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) { 
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };
}

String.prototype.trimLeft = function(charlist) {
    charlist = !charlist ? ' \\s\u00A0' : (charlist + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '$1');
    var re = new RegExp('^[' + charlist + ']+', 'g');
    return (this + '').replace(re, '');
};
String.prototype.trimRight = function(charlist) {
    charlist = !charlist ? ' \\s\u00A0' : (charlist + '').replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\\$1');
    var re = new RegExp('[' + charlist + ']+$', 'g');
    return (this + '').replace(re, '');
};
String.prototype.trimAll = String.prototype.allTrim || function(){
        return this.replace(/\s+/g,' ')
                   .replace(/^\s+|\s+$/,'');
     };
String.prototype.isAlphaNumeric = function () {
    var code, i, len;
    for (i = 0, len = this.length; i < len; i++) {
        code = this.charCodeAt(i);
        if (!(code > 47 && code < 58) && // numeric (0-9)
            !(code > 64 && code < 91) && // upper alpha (A-Z)
            !(code > 96 && code < 123)) { // lower alpha (a-z)
            return false;
        }
    }
    return true;
};
// Trim each item in array
Array.prototype.trim = function() {
    for(var i = this.length - 1; i >= 0; i--) {
        this[i].trim();
    }
};
Array.prototype.has = function(value) {
    for (var i = this.length - 1; i >= 0; i--) {
        if (this[i] == value) {
            return true;
        }
    }
    return false;
};

Array.prototype.pushUnique = function(item){
    if(item.isArray()){
        for (var j = item.length - 1; j >= 0; j--) {
            this.pushUnique(item[j]);
        }
        return;
    }
    if(this.indexOf(item) == -1){
        this.push(item);
        console.log('[INFO] Adding ' + item);
    }
};

// Array Remove - By John Resig (MIT Licensed)
// Remove the second item from the array
//array.remove(1);
// Remove the second-to-last item from the array
//array.remove(-2);
// Remove the second and third items from the array
//array.remove(1,2);
// Remove the last and second-to-last items from the array
//array.remove(-2,-1);
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

Array.prototype.removeValue = function(val) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === val) {
            this.splice(i, 1);
            i--;
        }
    }
    return this;
}

Object.prototype.getKeyByValue = function( value ) {
    for( var prop in this ) {
        if( this.hasOwnProperty( prop ) ) {
             if( this[ prop ] === value )
                 return prop;
        }
    }
};


$.getUrlVars = function () {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    
    for (var i = hashes.length - 1; i >= 0; i--) {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = decodeURIComponent(hash[1]);
    }
    return vars;
};

$.getUrlHash = function () {
    return window.location.href.slice(window.location.href.indexOf('#') + 1).split('&');
}

$.ruid = function (length, charSet) {
    var result = [];
    length = length || 5;
    length += 1;
    charSet = charSet || 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    while (--length) {
        result.push(charSet.charAt(Math.floor(Math.random() * charSet.length)));
    }
    return result.join('');
};

$.WS = function (link) {
    // socket.readyState:
    // CONNECTING = 0;
    // OPEN = 1;
    // CLOSED = 2;
    if(link !== undefined){
        if ('WebSocket' in window) {
            $.ws = new WebSocket(link);
            console.log('WebSocket OK');
        }else{
            console.log('No WebSocket');
        }
    }
    var close = function(){
        socket.disconnect();
    };
};
