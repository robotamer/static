'use strict'
if(typeof $ != 'object') var $ = {};
// $.src.js('ajax');
$.i18n = {};
$.i18n.store = {};
$.i18n.language = function (lang){
    var h = document.getElementsByTagName('html');
    if(lang){
        h[0].lang = lang;
    }
    return h[0].lang;
};
$.i18n.getJson = function () {
    var ajax = new Ajax();
    ajax.Responce = function(result){
	    $.db.object('language', result);
		$.i18n.store = result;
		$.i18n.translateAll();
	};
    ajax.Send('http://static.' + $.Domain() + '/json/lang/'+ $.i18n.language() +'.json');
};

$.i18n.init = function(lang){
    if(lang){
        $.i18n.language(lang);
    }
    var o = $.db.object('language');
    if(o.isEmpty()){
        $.i18n.getJson();
    }else{
        $.i18n.store = o;
        $.i18n.translateAll();
    }
};
$.i18n.Switch = function(language){
    $.i18n.language(language);
    $.i18n.getJson();
};
$.i18n.Translate = function(k){
    return $.i18n.store[k];
};
$.i18n.translateAll = function(){
    var es = document.body.getElementsByTagName('trans');
    for (var i = 0, l = es.length; i < l; i++){
        var e = es[i];
        var name = e.getAttribute('name');
        var trans = $.i18n.Translate(name);
        if(trans){
            e.removeChild(e.firstChild);
            e.appendChild(document.createTextNode(trans));
            e.lang = $.i18n.language();
        }else{
            e.lang = 'en';
        }
    }
};
$.i18n.export = function(){
    var es = document.body.getElementsByTagName('trans');
    var j = {};
    for (var i = 0, l = es.length; i < l; i++){
        var e = es[i];
        var name = e.getAttribute('name');
        j[name] = e.firstChild.nodeValue;
    }
    var t = document.getElementById('Translate');
    t.value = JSON.stringify(j, null, '  ');
};
var _ = $.i18n.Translate;
