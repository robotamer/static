function createCookie(name, value, days) {
  if (days) {
    var date = new Date();
    date.setTime(date.getTime()+(days*24*60*60*1000));
    var expires = "; expires="+date.toGMTString();
    }
  else var expires = "";
  document.cookie = name+"="+value+expires+"; path=/";
}

var date = new Date();
var gmt = date.getTimezoneOffset()/60;
createCookie("TimeTamerGMT", gmt, "365");
createCookie("TimeTamerTZjs", date, "365");

