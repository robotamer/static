'use strict'
if(typeof $ != 'object') var $ = {};
$.search = {};
$.src.js('ajax');



$.search.SuggestInit = function () {
    var list = $.db.getArray('keywords');
    new Suggest.LocalMulti("SearchInput", "suggest", list, {dispAllKey: true});
};

$.search.Query = function (input) {

    var ajax, query;

    if (input == undefined) return ;

    input.value.replace(/\W+/g, " ");
    if(input.value.isEmpty()){
        console.log('empty');
        return ;
    }else if(input.value.length < 3){
        console.log('less then 3');
        return ;      
    }
    $.search.term = input.value;
    var form = localStorage.getObj('settings');
    form['Searchterm'] = input.value;

    var sr = document.getElementById("SearchResult");
    $.dom.RemoveChildren(sr);

    query = "search=" + JSON.stringify(form);
    ajax = new Ajax();
    ajax.Responce = function (results) {

        if($.search.term.isEmpty()) return;

        // Save keywords in local strorage
        var keywords = $.search.term.split(" ");
        var keys = [];
        for (var i = 0, l = keywords.length; i < l; i++) {
            var k = keywords[i];
            k.trim();
            if (k.length > 2) {
                keys.push(k);
            }
        }

        if(keys.isEmpty()) return;

        sessionStorage.clear();
        sessionStorage.setObj('search', $.search.term);
        localStorage.addArray('history', $.search.term);
        //keywordsAdd(keys);

        // Save results by id in session storage
        if(results == null){
            $.dom.AppenedText('SearchResult', 'No results were found', 'h3');
        }else{
            for(var i = 0, l = results.length; i < l; i++) {
                var result = results[i];
                result['Tags'] = keys;
                var id = md5(result['Link']);
                result['Content'] = result['Content'].replace(/(<([^>]+)>)/ig,"");
                sessionStorage.addArray('ids', id);
                sessionStorage.object(id, result);
                result['id'] = id;
                console.log('[INFO] Engine: ' + result['Engine']);
                $.search.View(result);
            }            
        }
    };
    ajax.Send('/ajax', query, 'post');
};

$.search.Reload = function () {

	var search = sessionStorage.getObj('search');
	var ids = sessionStorage.getArray('ids');

	if(!search) return;
	if(!ids) return;

    // Save results by id in session storage
	for(var i = 0, l = ids.length; i < l; i++) {
		var id = ids[i];
		var result = sessionStorage.getObj(id);
		result['id'] = id;
	    $.search.View(result);
	}
};


$.search.View = function (object) {
    var col1 = document.createElement('div');
    var col2 = document.createElement('div');


    var divtitle = document.createElement('span');

    // Remove html from Title
    var div = document.createElement("div");
    div.innerHTML = object.Title;
    var title = div.textContent || div.innerText || "";

    // Create title with link
    var link = $.dom.Link(title, object.Link, true);

    var domain = document.createElement('cite');
    domain.setAttribute("class", "hostname");
    domain.appendChild(document.createTextNode($.domain(object.Link)));

    // Engine
    var engine = document.createElement('i');
    engine.setAttribute('title', object.Engine);
    engine.style.backgroundColor = 'gray';
    engine.style.color = 'white';
    engine.style.padding = '0.2em 0.6em 0.2em 0.6em' ;
    engine.style.cssFloat = 'right';
    $.dom.AppenedText(engine, object.Engine);

    // Load DIVs
    $.dom.AppendSpace(divtitle);
    divtitle.appendChild(link);
    $.dom.AppendSpace(divtitle);
    divtitle.appendChild(domain);
    $.dom.AppendSpace(divtitle);

    divtitle.appendChild(engine);

    col2.innerHTML = object.Content;

    // Save 
    var btnsave = document.createElement('i');
    btnsave.className = 'icon-bookmark-empty';
    btnsave.style.cursor = 'pointer';
    btnsave.setAttribute("onclick", "$.search.ToBook(this, '{0}');".format(object.id));
    col1.appendChild(btnsave);

/*
    // Navigation TD
    var engine = document.createElement('i');
    engine.setAttribute('title', object.Engine);
	engine.className = 'icon-' + object.Engine;
    col1.appendChild(engine);
*/
    // Load TDs
    col1.appendChild(divtitle);

    // Add all to TR 
    var bs = document.getElementById("SearchResult");
    bs.appendChild(col1);
    bs.appendChild(col2);
    bs.appendChild(document.createElement('hr'));
};

$.search.ToBook = function (e, id) { // function saveLink(e, id){
	var o = sessionStorage.getObj(id);
	o.Title = o.Title.replace(/(<([^>]+)>)/ig, '');
	var w = new Wire();
	w.Load(o.Title, o.Link, o.Tags, o.Content);
	w.Save();
	w.View();

    e.className = 'icon-bookmark';
    e.style.cursor = 'default';
    e.removeAttribute('onclick');
};
