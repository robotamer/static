'use strict';
if(typeof $ != 'object') var $ = {};
$.ajax = {};
/*
Must call with new:
    var ajax = new Ajax();
    ajax.Responce = function(){};
    ajax.Send(url, query, method);
*/
function Ajax() {
    var me = this;
    var spinner, spin, spinStatus;

    me.spinner = document.getElementById('ajaxspin');
    me.spin = document.createElement('i');
    me.spin.className = 'icon-spin';

    this.Responce = function(){};

    this.Start = function () {
        me.spinner.appendChild(me.spin);
        me.spinStatus = true;
    };

    this.Stop = function () {
        if(me.spinStatus){
            me.spinner.removeChild(me.spin);
        }
    };

    this.Request = function () {
        var e, r;
        try {
            // Opera 8.0+, Firefox, Safari
            r = new XMLHttpRequest();
        } catch (e) {
            console.log('XMLHttpRequest' + e);
            // Internet Explorer Browsers
            try {
                r = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                console.log('Msxml2.XMLHTTP' + e);
                try {
                    r = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    console.log('Microsoft.XMLHTTP' + e);
                    return null;
                }
            }
        }
        return r;
    };

    // var query = "?alias=" + form.alias + "&pass=" + form.pass + "&confirm=" + form.confirm;
    this.Send = function (url, query, method) {

        if(query == undefined) query = '';
        if(method == undefined) method = 'get';
        
        var connection = me.Request();

        connection.onreadystatechange = function() {
            switch (connection.readyState) {
            case 0:
                console.log('connection requesting');
                me.Start();
                me.spin.style.color = 'yellow';
                break;
            case 1:
                console.log('connection established');
                break;
            case 2:
                console.log('connection initialized');
                me.spin.style.color = 'green';
                break;
            case 3:
                console.log('processing request');
                break;
            case 4:
                console.log('connection closed and response is ready');
                if(connection.status == 200){
                    //console.log(connection.responseText);
                    me.Responce(JSON.parse(connection.responseText));
                }else{
                    console.log('connection status error: ' + connection.status);
                    console.log('connection response Text: ' + connection.responseText);
                }
                me.Stop();
                break;
            }
        };
        try {
            if(method == 'post'){ // && method == 'post'
                console.log('sending post request');
                connection.open("POST", url, true);
                connection.setRequestHeader("Content-type","application/x-www-form-urlencoded");
                connection.send(query); // "search=" + JSON.stringify(form)
            }else{
                console.log('sending get request');
                connection.open("GET", url + query, true);
                connection.send(null);                
            }
            console.log('sending query' + query);
        } catch (e) {
            console.log("open or send error: " + e);
        }
    };
}
