'use strict'
$.wire = {};

$.domain = function (url) {
    var p = document.createElement('a');
    p.href = url;
    p.hostname = p.hostname.toLowerCase();
    if (p.hostname.startsWith('www.')) {
        p.hostname = p.hostname.substring(4, p.hostname.length);
    }
    return p.hostname;
}

function Wire(){
	var Title, Link, Tags, Content, User, Date;

	this.TagsFix = function (){
		if(this.Tags){
			if(this.Tags.isString()){
				this.Tags.trim();
				this.Tags = this.Tags.replace(/,/g , ' ').replace(/  /g , ' ');
				this.Tags = this.Tags.split(' ');
			}
			if(this.Tags.isArray()){
				if(this.Tags.length > 5){
					this.Tags = this.Tags.slice(0, 5);
				} 				
			}
		}else{
			this.Tags = [];
		}
	};

	this.Load = function (title, link, tags, desc, user, id, date){
		link = link.trim();
    	this.Title = title.trim();
    	this.Link = link.trimRight('/#');
    	this.Tags = tags;
    	this.TagsFix();
    	this.Desc = (desc) ? desc : undefined;
    	this.Date = (date != undefined && date.isDate()) ? date :  $.date.iso();
    	this.User = (user) ? user : undefined;
	};

	this.Clear =function (){
		this.Title = undefined;
		this.Link  = undefined;
		this.Tags  = undefined;
		this.Date  = undefined;
		this.User  = undefined;
		this.Desc  = undefined;
	};

	this.Id = function (link){
		return (link) ? md5(link) : md5(this.Link);
	};

	this.Get = function (id){
		var o = $.db.getObj(id);
		this.Clear();
		var keys = Object.keys(o);
	    for (var i = keys.length - 1; i >= 0; i--) {
	        this[keys[i]] = o[keys[i]];
	    }
	};

	this.Save = function (){
		var id = this.Id();
		$.db.addArray('wires', id);
		$.db.setObj(id, this);
	};

	this.Delete = function (){
		$.db.delArray('wires', this.Id());
		$.db.removeItem(this.Id());
	};

	this.View = function (){

		var ele, id;

		var id = this.Id();

		var td1 = document.createElement('span');
		td1.className = "col";
		td1.style.width = '2em';


		// Vote
	    var vote = $.db.inArray('votes', id);
	    var	i = document.createElement('i');
    	i.setAttribute('title', 'thumbs');
        i.setAttribute('onclick', "WireVote(this,'{0}');".format(id));
        i.style.cursor = 'pointer';
        var t = (vote === true) ? 'icon-thumbs-uped' : 'icon-thumbs-up';
		i.setAttribute("class", t);
		td1.appendChild(i);

		// Delete
	    var i = document.createElement('i');
	    i.className = 'icon-trash';
	    i.setAttribute('title', 'Delete');
	    i.style.cursor = 'pointer';
		i.setAttribute('onclick', "WireDelete('{0}');".format(id));
		td1.appendChild(i);
		$.dom.AppendSpace(td1);

		var td2 = document.createElement('span');
		td2.className = "col";

		// Link
		var link = $.dom.Link(this.Title, this.Link, true);
		link.setAttribute('title', this.Desc);
		ele = document.createElement('span');
		ele.setAttribute('id', 'title_'+ id);
		ele.setAttribute('class', 'Link');
		ele.appendChild(link);
		td2.appendChild(ele);

		// Hostname
		var h = $.domain(this.Link);
		ele = document.createElement('sup');
		ele.style.fontSize = '.7em';
		ele.appendChild(document.createTextNode(h));
		ele.setAttribute('class', 'Info');
		$.dom.AppendSpace(td2, 2);
		td2.appendChild(ele);
		td2.appendChild(document.createElement('br'));

		// User
		var u = (this.User) ? this.User : 'Me';
		ele = document.createElement('small');
		$.dom.AppenedText(ele, u);
		i = document.createElement('i');
		i.setAttribute('class', 'icon-user Info');
		i.appendChild(ele);
		td2.appendChild(i);

		// Date
		var date = $.dom.DateSince(this.Date);
		ele = document.createElement('i');
		ele.style.fontSize = '.7em';
		ele.setAttribute('class', 'icon-time-ago Info');
		ele.appendChild(date);
		$.dom.AppendSpace(td2, 2);
		td2.appendChild(ele);

		// Tags
		ele = this.TagsView();
		$.dom.AppendSpace(td2, 2);
		td2.appendChild(ele);

		// Body
		var row = document.createElement('div');
		row.className = "row";
		row.style.padding = '0.5em';
		row.setAttribute("id", "news_" + id);
		row.appendChild(td1);
		row.appendChild(td2);

		ele = document.getElementById("bodyWire");
		ele.appendChild(row);
	};
}

Wire.prototype.TagsString = function() {
	var s = '';
	for (var i = 0, l = this.Tags.length; i < l; i++) {
	    s += this.Tags[i] + ' ';
	}
	return s;
};

Wire.prototype.TagsView = function() {
    var s = this.TagsString(this.Tags);
    var e = document.createElement('i');
    e.style.cursor = 'context-menu';
    e.setAttribute('class', 'icon-tag Info Tags');
    e.setAttribute('title', 'Tags');
    e.setAttribute('onclick', "Tags(this, '{0}');".format(this.Id()));
    e.appendChild($.dom.Link(s, '#'));
    return e;
};

Wire.prototype.TagsAdd = function (that, id) {
	this.Get(id);
	that.value = that.value.trim();
	this.Tags = that.value.split(' ');
	this.Tags.trim(); // Trim each value in array
	this.Save();
	var e = this.TagsView();
	var p = that.parentNode;            
	p.appendChild(e);
	p.removeChild(that);
};


Wire.prototype.TagsEdit = function (that, id) {
    var p = that.parentNode;
    p.removeChild(that);
    this.Get(id);
    var e = document.createElement('input');
    e.setAttribute('placeholder', 'keywords');
    e.setAttribute('name', 'tags');
    e.setAttribute('value', this.TagsString(this.Tags));
    e.setAttribute('onchange', "Tags(this, '{0}');".format(id));
    e.setAttribute('onblur', "Tags(this, '{0}');".format(id));
    p.appendChild(e);
};


function Tags(that, id){
	var w = new Wire();
	var x = that.nodeName;

	if (x.toLowerCase() == 'input') {
	    w.TagsAdd(that, id);
	}else if (x.toLowerCase() == 'i') {
	    w.TagsEdit(that, id);
	}else{
		console.log('Tags Error, id: ' + id);
	}
}

// Adds a bookmark via URL Get
function WireAdd(title, link, keys, desc) {
	if(title && link && title.isString() && link.isString()) {
		$.log.Info('Loading Link: ' + title);
		var w = new Wire();
		w.Load(title, link, keys, desc);
		w.Save();
		//w.View();
	 	window.location = 'http://www.pzzazz.com';
	 	window.location.assign('http://www.pzzazz.com');
	}
}

function WireVote(that, id) {
    var atr = that.getAttribute('class');
    if(atr == 'icon-thumbs-up'){
        localStorage.addArray('votes', id);
        that.setAttribute("class", 'icon-thumbs-uped');        
    }else{
        localStorage.delArray('votes', id);
        that.setAttribute("class", 'icon-thumbs-up');        
    }
}

Wire.prototype.Wires = function (cat) {

	$.dom.RemoveChildren('bodyWire');

    cat = cat ? cat : '';
    cat = cat.trim();

	var wires = $.db.getArray('wires');
	for (var i = wires.length - 1; i >= 0; i--) {
	    var wire = $.db.getArray(wires[i]);

        if (cat == '') {
		    wire['Id'] = wires[i];
		    this.Get(wire['Id']);
			this.View();
        } else if (wire["Tags"] && wire["Tags"].has(cat)) {
		    wire['Id'] = wires[i];
		    this.Get(wire['Id']);
			this.View();
        }
	}
};

function WireDelete(id){
	var r = confirm(_('ays'));
	if (r == false) return; 
	var d = document.getElementById('title_'+id);
	d.style.textDecoration = 'line-through';
	localStorage.removeItem(id);
	localStorage.delArray('wires', id);
	window.setTimeout($.dom.Remove('news_'+id), 2000);
}

function viewWires(cat) {
	var w = new Wire();
	w.Wires(cat);
}

Wire.prototype.keywordsReset = function () {
    $.db.Delete('keywords');
    $.db.Delete('keywordCount');
	var wires = $.db.getArray('wires');
	for (var i = wires.length - 1; i >= 0; i--) {
		var wire = $.db.getArray(wires[i]);
		this.keywordsAdd(wire['Tags']);
	}	
};

// Save keywords in local strorage
Wire.prototype.keywordsAdd = function (keywords) {
    var array = localStorage.getArray('keywords');
    var count = localStorage.getArray('keywordCount');
    for (var i = 0, l = keywords.length; i < l; i++) {
        var word = keywords[i];
        var index = array.indexOf(word);
        if(index == -1){
            array.push(word);
            count.push(1);
        }else{
            count[index] = count[index] + 1;
        }
    }
    localStorage.setArray('keywords', array);
    localStorage.setArray('keywordCount', count);
};

Wire.prototype.keywordsSort = function () {
    var words = $.db.getArray('keywords');
    var count = $.db.getArray('keywordCount');
    var length = count.length;
    var oList = [];

    for (var i = 0; i < length; i++) {
        var o = {};
        o['k'] = words[i];
        o['v'] = count[i];
        oList.push(o);
    }

    oList.sort(function(a, b) {
        return ((a.v > b.v) ? -1 : ((a.v == b.v) ? 0 : 1));
    });

    var w = [];
    var c = [];
    for (var i = 0; i < length; i++) {
        w.push(oList[i].k);
        c.push(oList[i].v);
    }
    $.db.setArray('keywords', w);
    $.db.setArray('keywordCount', c);
};

Wire.prototype.filterLoad = function () {
    var element = document.getElementById('datalist-category');    
    var words = $.db.getArray('keywords');
    var l = (words.length < 10) ? words.length : 10;
    $.dom.RemoveChildren(element);
    for (var i = 0; i < l; i++) {
        var option = document.createElement('option');
        option.value = words[i];
        element.appendChild(option);
    }
};


